"""vk_bot URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.contrib.auth import views as auth_views

from bot.views import HomeView, AuthView, ProfileTasksView, AddPage, AddTask, InfoTaskView

urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'', HomeView.as_view()),
    re_path(r'^login/$', auth_views.login, {"template_name": "login.html"}, name='login'),
    re_path(r'^logout/$', auth_views.logout, {'next_page': '/login'}, name='logout'),
    re_path(r'^vk-auth/$', AuthView.as_view(), name='vk-auth'),
    re_path(r'^profile/(?P<pk>[0-9]+)/$', ProfileTasksView.as_view(), name='profile'),
    re_path(r'^profile/(?P<pk>[0-9]+)/add_page/$', AddPage.as_view(), name='add_page'),
    re_path(r'^page/(?P<pk>[0-9]+)/add_task/$', AddTask.as_view(), name='add_task'),
    re_path(r'^task/(?P<pk>[0-9]+)/$', InfoTaskView.as_view(), name='info_task'),
]
