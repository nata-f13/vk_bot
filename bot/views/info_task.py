from django.shortcuts import render, redirect, reverse
from django.views import View, generic
from django.contrib.auth.mixins import LoginRequiredMixin

from bot.models import Task


class InfoTaskView(LoginRequiredMixin, generic.DetailView):
    model = Task
    login_url = '/login/'
    template_name = "info_task.html"

