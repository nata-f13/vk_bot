from django.shortcuts import render, redirect, reverse
from django.views import View, generic
from django.contrib.auth.mixins import LoginRequiredMixin

from bot.models import VkProfiles

__all__ = ['HomeView', 'ProfileTasksView']



class HomeView(LoginRequiredMixin, View):
    login_url = '/login/'
    template_name = "home.html"

    def get(self, request):
        profiles = request.user.profiles.all()
        if profiles:
            return redirect(reverse('profile', kwargs={'pk': profiles[0].pk}))
        return render(request, self.template_name)


class ProfileTasksView(LoginRequiredMixin, generic.DetailView):
    model = VkProfiles
    login_url = '/login/'
    template_name = "profile_tasks.html"





    # def get(self, request):
    #     # <view logic>
    #     return render(request, self.template_name)

