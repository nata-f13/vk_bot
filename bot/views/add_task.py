from django.shortcuts import render, reverse, redirect
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin

from bot.forms import TaskForm
from bot.models import Page, Task


class AddTask(LoginRequiredMixin, View):
    """
    Добавление задач
    """
    login_url = '/login/'
    template_name = "add_task.html"

    def get(self, request, **kwargs):
        form = TaskForm()
        return render(request, self.template_name, context={"form": form})

    def post(self, request, pk):
        # получение текущего профиля VK
        page = Page.objects.get(pk=pk)
        # передача данных из post-запроса в форму для валидации
        form = TaskForm(request.POST)
        form.page = page
        # запуск процессов валидации
        if form.is_valid():
            # запись в таблицу Task в БД
            task = Task(
                name=form.cleaned_data["name"],
                start_date=form.cleaned_data["start_date"],
                end_date=form.cleaned_data["end_date"],
                topic=form.cleaned_data["topic_link"],
                like_score=form.cleaned_data["like_score"],
                comment_score=form.cleaned_data["comment_score"],
                repost_score=form.cleaned_data["repost_score"],
                page=page
            )
            task.save()
            # переход на страницу профиля
            return redirect(reverse('profile', kwargs={'pk': page.profile.pk}))
        # отрисовка текущей страницы в случае ошибки
        return render(request, self.template_name, context={"form": form})
