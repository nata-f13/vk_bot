from django.shortcuts import render, redirect, reverse
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin

from bot.forms import PageForm
from bot.models import VkProfiles, Page


class AddPage(LoginRequiredMixin, View):
    """
    Добавление групп
    """
    login_url = '/login/'
    template_name = "add_pages.html"

    def get(self, request, **kwargs):
        form = PageForm()
        # отрисовка страницы с формой
        return render(request, self.template_name, context={"form": form})

    def post(self, request, pk):
        # получение текущего профиля VK
        profile = VkProfiles.objects.get(pk=pk)
        # передача данных из post-запроса в форму для валидации
        form = PageForm(request.POST)

        form.profile = profile
        # запуск процессов валидации
        if form.is_valid():
            # получение валидированной/измененной в методе clean_link информации (dict)
            data = form.cleaned_data['link']
            # запись в таблицу Page в БД
            page = Page(profile=profile, remote_id=data['id'], name=data['name'], image=data['image'])
            page.save()
            # переход на страницу профиля
            return redirect(reverse('profile', kwargs={'pk': profile.pk}))
        # отрисовка текущей страницы в случае ошибки
        return render(request, self.template_name, context={'form': form})