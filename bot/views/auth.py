from django.shortcuts import render, redirect
from django.views import View

from bot.forms import VkProfilesForm
from bot.models import VkProfiles

__all__ = ['AuthView']


class AuthView(View):
    """
    Авторизация VK профиля
    """
    template_name = "vk_auth.html"

    def get(self, request):
        form = VkProfilesForm()
        return render(request, self.template_name, context={'form': form})

    def post(self, request):
        # передача данных из post-запроса в форму для валидации
        form = VkProfilesForm(request.POST)
        # запуск процессов валидации
        if form.is_valid():
            # получение валидированной/измененной в методе clean_password информации
            login = form.cleaned_data['login']
            password = form.cleaned_data['password']
            # запись в таблицу VkProfiles в БД
            profile = VkProfiles(login=login, user=request.user)
            profile.set_password(password)
            profile.save()
            # переход на страницу
            # TODO: дописать комменты во вьюхах
            return redirect('/')

        return render(request, self.template_name, context={'form': form})