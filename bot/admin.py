from django.contrib import admin

from .models import VkProfiles, Page, Post, Action, Task

admin.site.register(VkProfiles, admin.ModelAdmin)
admin.site.register(Page, admin.ModelAdmin)
admin.site.register(Post, admin.ModelAdmin)
admin.site.register(Action, admin.ModelAdmin)
admin.site.register(Task, admin.ModelAdmin)