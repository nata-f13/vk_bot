import re

from django.forms import (
    Form, CharField, PasswordInput,
    ValidationError, DateField, IntegerField
)
from vk_api.exceptions import BadPassword, ApiError

from bot.vk_client import VkApi


class VkProfilesForm(Form):
    """
    Форма обработки данных профиля VK
    """
    login = CharField(max_length=100)
    password = CharField(widget=PasswordInput)

    def clean_password(self):
        """
        Валидатор пароля
        :return str: password
        """
        # получение значений полей, прошедших текстовую валидацию
        login = self.cleaned_data['login']
        password = self.cleaned_data['password']
        try:
            # попытка авторизации по полученным данным
            VkApi(login=login, password=password)
        except BadPassword:
            # если пароль неверный
            raise ValidationError('Неверный пароль')
        except Exception as e:
            # любая другая ошибка при авторизации
            raise ValidationError('Неизвестная ошибка')
        return password


class PageForm(Form):
    """
    Форма добавления группы
    """
    link = CharField(max_length=512)

    def clean_link(self):
        """
        Валидирование ссылки на группу
        :return dict page: словарь, содержащий информацию о введенной группе
        """
        # получение значения поля, прошедшего текстовую валидацию
        link = self.cleaned_data['link']
        # попытка авторизации VK
        login = self.profile.login
        password = self.profile.get_password
        vk = VkApi(login=login, password=password)
        try:
            # попытка получить информацию о группе
            page = vk.get_page(page_link=link)
        except ApiError:
            # любая ошибка в результате выполнения метода Vk_Api
            raise ValidationError("Неверная ссылка")
        else:
            # проверка - является ли авторизованный профиль VK админом введенной группы
            if not page["is_admin"]:
                raise ValidationError("Вы не являетесь администратором группы")
        return page


class TaskForm(Form):
    """
    Форма создания задачи
    """
    name = CharField(max_length=512)
    topic_link = CharField(max_length=512)
    start_date = DateField(input_formats=['%d.%m.%Y'])
    end_date = DateField(input_formats=['%d.%m.%Y'])
    like_score = IntegerField()
    comment_score = IntegerField()
    repost_score = IntegerField(required=False)

    def clean_topic_link(self):
        """
        Валидатор ссылки на обсуждение
        :return int topic: идентификатор обсуждения
        """
        # получение значения поля, прошедшего текстовую валидацию
        link = self.cleaned_data['topic_link']
        try:
            # получение идентификатора обсуждения из ссылки на обсуждение
            topic = int(re.match(".*topic-(\d+)_(\d+)", link).group(2))
        except:
            raise ValidationError("Неверная ссылка")

        profile = self.page.profile
        # попытка авторизации VK
        login = profile.login
        password = profile.get_password
        vk = VkApi(login=login, password=password)
        try:
            # валидация ссылки на обсуждение с помощью
            # метода получения комментарии пользователей в указанном обсуждении
            vk.get_topic_comments(group_id=self.page.remote_id, topic_id=topic, start_comment_id=0)
        except ApiError:
            raise ValidationError("Неверная ссылка")

        return topic


