
import base64
from operator import itemgetter

from django.db import models
from django.contrib.auth.models import User
from Crypto.Cipher import DES, AES
from django.conf import settings


class VkProfiles(models.Model):
    login = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='profiles')

    def set_password(self, password):
        """Кодирование паролей для дальнейшего использования"""
        password = password.rjust(64)
        self.password = base64.b64encode(
            AES.new(
                settings.SECRET, AES.MODE_ECB
            ).encrypt(password)
        ).decode("utf-8")

    @property
    def get_password(self):
        """Декодирование паролей"""
        return AES.new(
            settings.SECRET, AES.MODE_ECB
        ).decrypt(base64.b64decode(self.password)).strip().decode("utf-8")

    def __str__(self):
        return self.login

    def get_pages(self):
        """
        Список всех подключенных групп профиля
        :return:
        """
        return self.page.order_by("-pk")


class Page(models.Model):
    """
    Таблица групп
    """
    profile = models.ForeignKey(VkProfiles, on_delete=models.CASCADE, related_name='page')
    remote_id = models.IntegerField()
    name = models.CharField(max_length=255)
    image = models.CharField(max_length=255, blank=True)

    def get_tasks(self):
        return self.tasks.order_by("-pk")


class Task(models.Model):
    """
    Таблица задач
    """
    name = models.CharField(max_length=512)
    start_date = models.DateField()
    end_date = models.DateField(blank=True)
    page = models.ForeignKey(Page, on_delete=models.CASCADE, related_name='tasks')
    topic = models.IntegerField()
    like_score = models.IntegerField()
    comment_score = models.IntegerField()
    repost_score = models.IntegerField(default=0)
    last_comment = models.IntegerField(blank=True, null=True)
    comment_marker = models.CharField(max_length=128)

    @property
    def likes(self):
        """
        Получение количества лайков
        :return int:
        """
        return self._action_count(ActionTypes.LIKE)

    @property
    def comments(self):
        """
        Получение количества комментариев
        :return int:
        """
        return self._action_count(ActionTypes.COMMENT)

    @property
    def reposts(self):
        """
        Получение количества репостов
        :return int:
        """
        return self._action_count(ActionTypes.REPOST)

    def _action_count(self, action_type: str) -> int:
        """
        :param str action_type: тип события
        :return int: количество событий по типу
        """
        return Action.objects.filter(
            type=action_type,
            post__page=self.page,
            post__date__gte=self.start_date,
            post__date__lte=self.end_date
        ).count()

    def get_user_score(self, user_id: int):
        """
        Функция возвращает информацию об активности участника и его место
        :param user_id: id участника
        :return int, dict: место в рейтинге, информация об активности
        """
        for num, info in self.get_users():
            if info['user_id'] == user_id:
                return num, info
        return None, None

    def get_users(self):
        """
        Функция расчета баллов для каждого участника,
        вычисления рейтинга и победителя
        """
        # фильтр событий по: принадлежности поста к странице, соответствию даты поста датам в задаче
        actions = Action.objects.values('user_id', 'type').filter(
            post__page=self.page,
            post__date__gte=self.start_date,
            post__date__lte=self.end_date
        ).annotate(count=models.Count('type'))

        result = {}

        # подсчет результативного балла участника
        for _action in actions:
            res = result.setdefault(
                _action['user_id'],
                {
                    'user_id': _action['user_id'],
                    'like': 0,
                    'comment': 0,
                    'repost': 0,
                    'score': 0
                }
            )
            score = res['score']
            count = _action['count']
            _type = _action['type']
            _type_score = getattr(self, f'{_type}_score') or 0

            score += _type_score * count

            res['score'] = score
            res[_type] = count * _type_score

        # сортировка участников по score
        result = list(result.values())
        result = sorted(result, key=itemgetter('score'), reverse=True)

        # нумерация участников
        result = [(num + 1, data) for num, data in enumerate(result)]

        return result




class ActionTypes:
    LIKE = 'like'
    COMMENT = 'comment'
    REPOST = 'repost'

    CHOICE = (
        (LIKE, "Like"),
        (COMMENT, "Comment"),
        (REPOST, "Repost"),
    )


class Post(models.Model):
    """
    Таблица постов
    """
    date = models.DateField()
    post_id = models.IntegerField()
    page = models.ForeignKey(Page, on_delete=models.CASCADE)

    class Meta:
        """
        Уникальность записи относительно двух параметров: идентификатор поста и идентификатор страницы
        """
        unique_together = ('post_id', 'page',)

    def _get_last(self, action_type):
        """
        Получение последнего действия для определенного поста по типу действия
        :param str action_type: тип действия
        :return Action: запись из таблицы Action
        """
        try:
            return self.action.filter(type=action_type).latest('id')
        except Action.DoesNotExist:
            return None


    @property
    def last_like(self):
        """
        Получение последнего обработанного лайка
        :return Action:
        """
        return self._get_last(ActionTypes.LIKE)

    @property
    def last_comment(self):
        """
        Получение последнего обработанного комментария
        :return Action:
        """
        return self._get_last(ActionTypes.COMMENT)

    @property
    def last_repost(self):
        """
        Получение последнего обработанного репоста
        :return Action:
        """
        return self._get_last(ActionTypes.REPOST)


class Action(models.Model):
    """
    Таблица действий (action)
    """
    type = models.CharField(choices=ActionTypes.CHOICE, max_length=32)
    action_id = models.IntegerField()
    user_id = models.IntegerField()
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='action')

