import datetime

from django.core.management.base import BaseCommand

from bot.models import Task, Post, Action, ActionTypes
from bot.vk_client import VkApi


class Command(BaseCommand):

    def handle(self, *args, **options):
        for task in Task.objects.filter(end_date__gte=datetime.date.today()):
            # получение профиля администратора группы VK
            profile = task.page.profile
            # авторизация VK
            vk = VkApi(login=profile.login, password=profile.get_password)
            # цикл по всем постам задачи
            for post_id, post_date in vk.get_posts(-task.page.remote_id, task.start_date, task.end_date):
                # запись в БД: получаем либо создаем запись в таблице Post; created - bool
                post, created = Post.objects.get_or_create(post_id=post_id, page=task.page, date=post_date)
                if created:
                    post.save()

                last_repost = post.last_repost
                reposts = vk.get_reposts(-task.page.remote_id, post_id, stop_id=last_repost.action_id if last_repost else None)
                self.save_actions(reposts, post, ActionTypes.REPOST)

                last_comment = post.last_comment
                comments = vk.get_comments(-task.page.remote_id, post_id, stop_id=last_comment.action_id if last_comment else None)
                self.save_actions(comments, post, ActionTypes.COMMENT)

                likes = vk.get_likers_by_post(-task.page.remote_id, post_id)
                self.save_actions(likes, post, ActionTypes.LIKE)

    def save_actions(self, items, post, action_type):
        """
        Создание записи в таблицу Actions в БД
        :param list items: список записываемых действий (action): репост, комментарий
        :param int post: идентификатор поста, которому принадлежит действие
        :param str action_type: тип действия
        :return None:
        """
        for item in items:
            action, created = Action.objects.get_or_create(
                type=action_type,
                action_id=item['action_id'],
                user_id=item['user_id'],
                post=post
            )
            if created:
                action.save()

