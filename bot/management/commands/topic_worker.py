import datetime
from uuid import uuid4
from time import sleep

from django.core.management.base import BaseCommand

from bot.models import Task
from bot.vk_client import VkApi


class Command(BaseCommand):
    """
    Функция получает все комментарии из обсуждения к задаче
    и отвечает на сообщения, содержащие маркер (comment_marker)
    """

    def handle(self, *args, **options):
        while True:
            for task in Task.objects.filter(end_date__gte=datetime.date.today()):
                # получение профиля администратора группы VK
                profile = task.page.profile
                # авторизация VK
                vk = VkApi(login=profile.login, password=profile.get_password)

                # получение необработанного списка комментариев в обсуждении конкурса
                for user_id, last_id, first_name, text in vk.get_topic_comments(
                        task.page.remote_id, task.topic,
                        start_comment_id=task.last_comment if task.last_comment else 0):

                    # фиксация последнего обработанного комментария
                    task.last_comment = last_id
                    task.save()

                    # ответ на комментарий по запросу
                    if task.comment_marker.lower() in text.lower():
                        vk.create_comment(
                            group_id=task.page.remote_id,
                            topic_id=task.topic,
                            message=self.get_message(last_id, first_name, user_id, task),
                            guid=str(uuid4())
                        )

            # ждем 30сек
            sleep(30)

    @staticmethod
    def get_message(post_id, first_name, user_id, task: Task):
        """
        Формирование ответа на комментарий в обсуждении
        :param post_id: для формирования ссылки в ответе на запрашивающего участника
        :param first_name: имя запрашивающего участника (также для ссылки)
        :param user_id: ID запрашивающего участника
        :param task: экземпляр модели Task
        :return str: текст сообщения
        """
        num, info = task.get_user_score(user_id)

        if num is not None and info is not None:
            return f"""[post{post_id}|{first_name}],
            У вас {info['score']} баллов, вы занимаете {num} место:
            - за лайки постов {info['like']} баллов
            - за комментарии {info['comment']} баллов."""
        else:
            return f"""[post{post_id}|{first_name}],
            К сожалению, у вас пока что 0 баллов. 
            Ставьте лайки, пишите комментарии и проверяйте свои баллы снова. 
            Убедитесь, что вы подписаны на наше сообщество."""