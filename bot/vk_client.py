from datetime import date

import vk_api


#vk_api.VkApi(token = 'a02d...e83fd') #Авторизоваться как сообщество

class VkApi:
    def __init__(self, login, password):
        self.instance = vk_api.VkApi(login=login, password=password)
        self.instance.auth()

    def _get_reposts(self, owner_id, post_id, offset=0, count=100):
        """
        Получение информации о репостах одного поста
        :param int owner_id: идентификатор сообщества, на стене которого находится запись
        :param int post_id: идентификатор записи на стене
        :param int offset: смещение, необходимое для выборки определенного подмножества записей.
        :param int count: количество записей, которое необходимо получить.
        :return: объект
        """
        return self.instance.method('wall.getReposts',
                                    {'owner_id': owner_id,
                                     'post_id': post_id,
                                     'offset': offset,
                                     'count': count}
                                    )

    def _get_comments(self, owner_id, post_id, offset=0, count=100, sort='asc', preview_length=1):
        """
        Получение информации о комментариях одного поста
        :param int owner_id: идентификатор сообщества, на стене которого находится запись
        :param int post_id: идентификатор записи на стене
        :param int offset: сдвиг, необходимый для получения конкретной выборки результатов
        :param int count: число комментариев, которые необходимо получить (max 100)
        :param str sort: порядок сортировки комментариев (asc - от старых к новым; desc — от новых к старым.)
        :param int preview_length: количество символов, по которому нужно обрезать текст комментария
        (нам не нужен сам комментарий, поэтому обрезаем максимально)
        :return:
        """
        return self.instance.method('wall.getComments',
                                    {'owner_id': owner_id,
                                     'post_id': post_id,
                                     'offset': offset,
                                     'count': count,
                                     'sort': sort,
                                     'preview_length': preview_length}
                                    )

    def get_posts(self, owner_id, date_start, date_end):
        """
        Получение списка постов
        :param int owner_id: идентификатор сообщества, со стены которого необходимо получить записи.
        Идентификатор сообщества необходимо указывать со знаком "-"
        :param date_start: временной промежуток, в который должен попадать каждый пост.
        :param date_end: временной промежуток, в который должен попадать каждый пост.
        :return list posts: список постов, содержащий для каждого поста: id поста, дата поста
        """
        offset = 0
        is_not_end = True

        posts = []
        while is_not_end:
            # берем по 100 записей - максимальное ограничение параметра count в методе wall.get
            response = self.instance.method('wall.get',
                                            {'owner_id': owner_id,
                                             'count': 100,
                                             'offset': offset}
                                            )
            for num, item in enumerate(response['items']):
                _date = date.fromtimestamp(item["date"])
                # условие проверки вхождения поста во временной диапазон
                if date_start <= _date <= date_end:
                    posts.append((item["id"], _date))
                # условие остановки обработки до первой даты, не попадающей во временной диапазон,
                # исключая первый пост (num = 0), который может быть закреплен на стене и может не попадать в даты
                elif date_start > _date and num > 0:
                    is_not_end = False
                    break

            if not response['items']:
                is_not_end = False
            # смещение относительно начала списка. может прийти не 100 записей
            offset += len(response['items'])

        return posts

    def get_likers_by_post(self, owner_id, item_id):
        """
        Получение лайков определенного поста
        :param int owner_id: идентификатор сообщества - автора поста.
        :param int item_id: идентификатор поста
        :return list list_actions: список лайкнувших пользователей
        """
        is_not_end = True
        list_actions = []
        offset = 0

        while is_not_end:
            # берем по 1000 лайкеров - максимальное ограничение count метода likes.getList
            response = self.instance.method('likes.getList',
                                            {'type': 'post',
                                             'owner_id': owner_id,
                                             'item_id': item_id,
                                             'filter': 'likes',
                                             'offset': offset,
                                             'count': 1000,
                                             'skip_own': 0}
                                            )

            for item in response['items']:
                list_actions.append({
                    "action_id": item,
                    "user_id": item
                })

            if not response['items']:
                is_not_end = False
            # смещение относительно начала списка
            offset += len(list_actions)

        return list_actions

    def get_reposts(self, owner_id, post_id, stop_id=None):
        """
        Получение необработанных репостов
        :param int owner_id: идентификатор сообщества - автора поста.
        :param int post_id: идентификатор поста
        :param int stop_id: идентификатор последнего лайкнувшего пользователя,
        чтобы при следующем получении лайков этого же поста не получать "старых" лайкеров
        :return:
        """
        return self.action_handler(self._get_reposts, owner_id, post_id, stop_id)

    def get_comments(self, owner_id, post_id, stop_id=None):
        """
        Получение необработанных комментариев
        :param int owner_id: идентификатор сообщества - автора поста.
        :param int post_id: идентификатор поста
        :param int stop_id: идентификатор последнего лайкнувшего пользователя,
        чтобы при следующем получении лайков этого же поста не получать "старых" лайкеров
        :return:
        """
        return self.action_handler(self._get_comments, owner_id, post_id, stop_id)

    def action_handler(self, action, owner_id, post_id, stop_id=None):
        """
        Обработчик действий (комментарии, репосты)
        :param def action: функция вызова соответствующего метода (_get_reposts либо _get_comments)
        :param int owner_id: идентификатор сообщества - автора поста.
        :param int post_id: идентификатор поста
        :param int stop_id: идентификатор последнего лайкнувшего пользователя,
        чтобы при следующем получении лайков этого же поста не получать "старых" лайкеров
        :return list list_actions: список пользователей (id, имя)
        """
        is_not_end = True
        list_actions = []
        offset = 0

        while is_not_end:
            response = action(owner_id=owner_id, post_id=post_id, offset=offset)

            for item in response['items']:
                # если дошли до идентификатора последнего обработанного ранее пользователя
                if stop_id == item['id']:
                    is_not_end = False
                    break
                list_actions.append({
                    "action_id": item['id'],
                    "user_id": item['from_id']
                })

            if not response['items']:
                is_not_end = False
            # смещение относительно начала списка
            offset = len(list_actions)

        return list_actions

    def get_page(self, page_link):
        """
        Получение информации о группе. Для двух целей:
        - проверка, является ли авторизованный пользователь сервиса админом группы
        - аватарка и имя группы для вывода в интерфейсе
        :param str page_link: идентификатор или короткое имя сообщества.
        :return dict:
        """
        response =  self.instance.method('groups.getById',
                                    {'group_ids': page_link})
        if response:
            group = response[0]
            return {
                "id": group["id"],
                "name": group["name"],
                "image": group.get("photo_100", group.get("photo_50", "")),
                "is_admin": group["is_admin"]
            }

    def get_topic_comments(self, group_id, topic_id, start_comment_id):
        """
        Получение всех комментариев в обсуждении
        :param int group_id: идентификатор сообщества
        :param int topic_id: идентификатор обсуждения
        :param int start_comment_id: идентификатор обсуждения
        :return list comments: список, содержащий инфу по каждому комментарию:
        id автора комментария, id поста, имя автора комментария, текст комментария
        """
        is_not_end = True
        offset = 0
        profiles = {}
        comments = []
        while is_not_end:
            # берем по 100 комментариев - максимальное ограничение count метода board.getComments
            response = self.instance.method('board.getComments',
                                        {'group_id': group_id,
                                         'topic_id': topic_id,
                                         'start_comment_id': start_comment_id,
                                         'offset': offset,
                                         'count': 100,
                                         'sort': 'asc',
                                         'extended': 1}
                                        )
            for p in response["profiles"]:
                # фиксация имени соответствующего id пользователя
                profiles.setdefault(p["id"], p["first_name"])

            for item in response["items"]:
                # проверка каждого комментария: не принадлежит группе и не последний обработанный ранее
                if abs(item["from_id"]) != group_id and item["id"] != start_comment_id:
                    comments.append((item["from_id"],
                                     item["id"],
                                     profiles[item["from_id"]],
                                     item["text"])
                                    )
            # смещение относительно начала списка
            offset = len(response['items'])
            if not response["items"]:
                is_not_end = False

        return comments

    def create_comment(self, group_id, topic_id, message, guid, from_group=1):
        """
        Создает комментарий-ответ
        :param int group_id: идентификатор сообщества
        :param int topic_id: идентификатор темы
        :param str message: текст комментария
        :param str guid: уникальный идентификатор комментария
        :param from_group: 1 — сообщение будет опубликовано от имени группы,
        0 — сообщение будет опубликовано от имени пользователя (по умолчанию).
        :return int: идентификатор созданного комментария
        """
        return self.instance.method('board.createComment',
                                    {'group_id': group_id,
                                     'topic_id': topic_id,
                                     'message': message,
                                     'guid': guid,
                                     'from_group': from_group}
                                    )
