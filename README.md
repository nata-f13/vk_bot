ИНСТРУКЦИЯ ДЛЯ ЗАПУСКА на Ubuntu 18.04 с версией Python 3.6

# Создание виртуального окружения:
    python3 -m venv venv

# Активация витруального окружения:
    source venv/bin/activate

# Установка зависимостей:
    pip install -r requirments.txt

# Конфигурирование базы данных:
    python manage.py makemigrations
    python manage.py migrate

# Создание администратора системы:
    python manage.py createsuperuser

# Запуск интерфейса:
    python manage.py runserver

# Запуск обработчика комментариев в обсуждении:
    python manage.py topic_worker

# Запуск обработчика задач:
    python manage.py task_worker